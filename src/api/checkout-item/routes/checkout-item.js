'use strict';

/**
 * checkout-item router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::checkout-item.checkout-item');
