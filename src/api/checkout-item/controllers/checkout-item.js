'use strict';

/**
 * checkout-item controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::checkout-item.checkout-item');
