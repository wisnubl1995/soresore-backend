'use strict';

/**
 * checkout-item service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::checkout-item.checkout-item');
